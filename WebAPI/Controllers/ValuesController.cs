﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    public class ValuesController : ApiController
    {
        // GET api/values
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        //public string Post([FromBody]string value)
        //{
        //    return value;
        //}

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
        public WebhookResponse Post(JObject result)
        {
            dynamic aa = result;
            var res = Newtonsoft.Json.JsonConvert.DeserializeObject<Result>(aa.result.ToString());
            Dictionary<string, object> myDict = new Dictionary<string, object>();

            string Topping = res.parameters.Topping;


            WebhookResponse jsonData = new WebhookResponse
            {
                speech = "Fred is the 37th and current President of the United States " + Topping,
                displayText = "Fred is the 64th and current President of the United States",
                //data = myDict,.. commented these two out for now can add later
                //contextOut = myCont,
                source = "agent"
            };

            string str = Newtonsoft.Json.JsonConvert.SerializeObject(jsonData);
            return jsonData;
        }


    }
}
