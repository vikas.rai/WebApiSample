﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models
{
    public class Metadata
    {
        public string intentId { get; set; }
        public string webhookUsed { get; set; }
        public string webhookForSlotFillingUsed { get; set; }
        public string intentName { get; set; }
    }
    public class Message
    {
        public int type { get; set; }
        public string speech { get; set; }
    }

    public class Fulfillment
    {
        public string speech { get; set; }
        public List<Message> messages { get; set; }
    }
    public class Parameters
    {
        public string SHIPMENT { get; set; }
        public string Topping { get; set; }
    }

    public class Result
    {
        public string source { get; set; }
        public string resolvedQuery { get; set; }
        public string action { get; set; }
        public bool actionIncomplete { get; set; }
        public Parameters parameters { get; set; }

    }


    public class WebhookResponse
    {


        /// <summary>
        /// Voice response to the request.
        /// </summary>
        public string speech { get; set; }

        /// <summary>
        /// Text displayed on the user device screen.
        /// </summary>
        public string displayText { get; set; }

        /// <summary>
        /// Additional data required for performing the action on the client side.
        /// The data is sent to the client in the original form and is not processed by Api.ai.
        /// </summary>
        //public Dictionary<string, object> data { get; set; }

        /// <summary>
        /// Array of context objects set after intent completion.
        /// </summary>
        //  public Context[] contextOut { get; set; }

        /// <summary>
        /// Data source
        /// </summary>
        public string source { get; set; }

    }
}